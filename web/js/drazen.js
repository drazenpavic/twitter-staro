jQuery(document).ready(function() {


    $('.loading').hide();
    var today = new Date();

    $('.shade').click(function() {
        $('.shade').hide();
    });

    $('.new_tweet').click(function() {
        $('.shade').show();
        $("#NewOrEdit").html("New Tweet");
        $("#tweetID").val("");
        $("#scheduleTweet").html("");
        var prettyDate = today.getDate() + '/' + (today.getMonth()+1) + '/' +
        today.getFullYear();
        $("#scheduleDate").val(prettyDate);
        $("#scheduleHour").val("00");
        $("#scheduleMinute").val("00");
        $('#scheduleDate').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: today
        });

    });

    $('#newtweet').click(function(event) {
        event.stopPropagation();
    });

    $('.delete').click(function() {
        var check = confirm('Are you sure you want to delete this post?');
        var path = $(this).attr('data-path');
        if (check == true) {
            $.post(path + '?id=' + $(this).attr('data-id'));
            $(this).parents('li').slideUp('slow');
        } else {
            return false;
        }
    });

    $('#scheduleDate').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: today
    });

    $('.edit').click(function() {
        $('.loading').show();
        var path = $(this).attr('data-path');
        $.post(path + '?id=' + $(this).attr('data-id'),
                function(data) {
                    console.log(data);
                    $("#NewOrEdit").html("Edit Tweet");
                    $("#tweetID").val(data.id);
                    $("#scheduleTweet").html(data.content);
                    $("#scheduleDate").val(data.date);
                    $("#scheduleHour").val(data.hour);
                    $("#scheduleMinute").val(data.minute);
                    $('#scheduleDate').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: today
                    });
                    $('.loading').hide(function() {
                        $('.shade').show();
                    });
                });
    });






});