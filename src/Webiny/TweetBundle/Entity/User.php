<?php

namespace Webiny\TweetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * User
 *
 * @ORM\Table("fos_user")
 * @ORM\Entity(repositoryClass="Webiny\TweetBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(name="twitter_id", type="string", length=255, nullable=true)
     */
    protected $twitter_id;
    
    /** 
     * @ORM\Column(name="twitter_access_token", type="string", length=255, nullable=true) 
     */
    protected $twitter_access_token;
    
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Tweets", mappedBy="user")
     */
    protected $tweets;
    
    public function __construct()
    {
        $this->tweets = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set twitter_id
     *
     * @param string $twitterId
     * @return User
     */
    public function setTwitterId($twitterId)
    {
        $this->twitter_id = $twitterId;
    
        return $this;
    }

    /**
     * Get twitter_id
     *
     * @return string 
     */
    public function getTwitterId()
    {
        return $this->twitter_id;
    }

    /**
     * Set twitter_access_token
     *
     * @param string $twitterAccessToken
     * @return User
     */
    public function setTwitterAccessToken($twitterAccessToken)
    {
        $this->twitter_access_token = $twitterAccessToken;
    
        return $this;
    }

    /**
     * Get twitter_access_token
     *
     * @return string 
     */
    public function getTwitterAccessToken()
    {
        return $this->twitter_access_token;
    }

    /**
     * Add tweets
     *
     * @param \Webiny\TweetBundle\Entity\Tweets $tweets
     * @return User
     */
    public function addTweet(\Webiny\TweetBundle\Entity\Tweets $tweets)
    {
        $this->tweets[] = $tweets;
    
        return $this;
    }

    /**
     * Remove tweets
     *
     * @param \Webiny\TweetBundle\Entity\Tweets $tweets
     */
    public function removeTweet(\Webiny\TweetBundle\Entity\Tweets $tweets)
    {
        $this->tweets->removeElement($tweets);
    }

    /**
     * Get tweets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTweets()
    {
        return $this->tweets;
    }
}