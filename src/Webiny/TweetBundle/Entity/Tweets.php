<?php

namespace Webiny\TweetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tweets
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Webiny\TweetBundle\Entity\TweetsRepository")
 */
class Tweets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish", type="datetime")
     */
    private $publish;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=140)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     */
    private $status;
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="tweets")
     *@ORM\JoinColumn(name="user_id", referencedColumnName="id") 
     */
    protected $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set content
     *
     * @param string $content
     * @return Tweets
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Tweets
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set publish
     *
     * @param \DateTime $publish
     * @return Tweets
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
    
        return $this;
    }

    /**
     * Get publish
     *
     * @return \DateTime 
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set user
     *
     * @param \Webiny\TweetBundle\Entity\User $user
     * @return Tweets
     */
    public function setUser(\Webiny\TweetBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Webiny\TweetBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}