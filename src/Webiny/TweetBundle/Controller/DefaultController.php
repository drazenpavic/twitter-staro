<?php

namespace Webiny\TweetBundle\Controller;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Webiny\TweetBundle\Entity\Tweets;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/dashboard", name="dashboard")
     * @Template()
     */
    public function dashboardAction()
    {
        /**
         * @var $repo TweetsRepository
         */
        $repo = $this->getDoctrine()->getRepository("WebinyTweetBundle:Tweets");
        $user = $this->getUser();

        $id = $user->getId();
        $data = $repo->createQueryBuilder('t')
                ->where('t.status = :status')
                ->andWhere('t.user = :id')
                ->setParameters(array(
                    'status' => 'queued',
                    'id' => $id))
                ->orderBy('t.publish', 'ASC')
                ->select()
                ->getQuery();
        $result = $data->getResult();


        return array('data' => $result, 'user' => $user);
    }

    /**
     * @Route("/dashboard/publish", name="publish")
     * 
     */
    public function publishAction()
    {
        $tweetId = $this->getRequest()->get('tweetID');
        $content = $this->getRequest()->get('content');
        $publish = $this->getRequest()->get('publish');

        $em = $this->getDoctrine()->getEntityManager();

        //if is set $tweetId => EDIT TWEET else => NEW TWWET
        if ($tweetId) {
            /* @var $tweet Tweets */
            $tweet = $em->getRepository("WebinyTweetBundle:Tweets")->find($tweetId);
            if (!$tweet) {
                throw $this->createNotFoundException('No tweet found! Try again!');
            }
            $user = $tweet->getUser();
        } else {
            $tweet = new Tweets();
            $user = $this->getUser();
        }
        
        $tweet->setContent($content);   
        
        /* @var $date \DateTime */
        $date = DateTime::createFromFormat("d/m/Y", $publish);
        $date->setTime($this->getRequest()->get('hour'), $this->getRequest()->get('minute'), 00);
        $tweet->setPublish($date);

        //$status = $this->status($date);
        $status = "queued";
        $tweet->setStatus($status);
        $tweet->setUser($user);

        $em->persist($tweet);
        $em->flush();

        return $this->redirect($this->generateUrl('dashboard', array('user' => $user)));
    }

    /**
     * @Route("/delete/", name="delete")
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->get('id');

        /**
         * @var $repo TweetRepository
         */
        $repo = $this->getDoctrine()->getRepository("WebinyTweetBundle:Tweets");
        $tweet = $repo->find($id);

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($tweet);
        $em->flush();

        return $this->redirect($this->generateUrl('dashboard'));
    }

    /**
     * @Route("/edit/", name="edit")
     */
    public function ajaxEditAction()
    {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getEntityManager();

        /* @var $tweet Tweets */
        $tweet = $em->getRepository("WebinyTweetBundle:Tweets")->find($id);

        $json = array(
            'id' => $tweet->getId(),
            'content' => $tweet->getContent(),
            'date' => $tweet->getPublish()->format("d/m/Y"),
            'hour' => $tweet->getPublish()->format('H'),
            'minute' => $tweet->getPublish()->format('i')
        );

        return new JsonResponse($json);
    }

}
