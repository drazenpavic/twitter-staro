<?php

namespace Acme\DemoBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GreetCommand extends ContainerAwareCommand
{
  //TODO: PROČITATI PUBLISH TWEET! 
    protected function configure()
    {
        $this->setName('demo:greet')
                ->setDescription('Greet someone')
                ->addArgument(
                        'name', InputArgument::OPTIONAL, 'Who do you want to greet?'
                )
                ->addOption(
                        'yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters'
                )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
        $id = $input->getArgument('name');
        $em = $this->getContainer()->get('doctrine');
        $repo = $em->getRepository("WebinyTweetBundle:Tweets");
       
        if ($id) {
            $data = $repo->createQueryBuilder('t')
                ->Where('t.user = :id')
                ->setParameters(array(
                    'id' => $id))
                ->select()
                ->getQuery();
        $result = $data->getResult();
        }
        else {
            $output->writeln("Hello");
        }

        if ($input->getOption('yell')) {
            $text = strtoupper($text);
        }
        
        foreach ($result as $tweet) {
            $output->writeln($tweet->getContent());
        }
        
    }
}
