<?php

/* WebinyTweetBundle:Default:dashboard.html.twig */
class __TwigTemplate_8f24b447aab66bea43631026aacbb86a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'bodyClassName' => array($this, 'block_bodyClassName'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
            'contentTitle2' => array($this, 'block_contentTitle2'),
            'content2' => array($this, 'block_content2'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Dashboard";
    }

    // line 5
    public function block_bodyClassName($context, array $blocks = array())
    {
        echo "backend dashboard";
    }

    // line 7
    public function block_contentTitle($context, array $blocks = array())
    {
        echo "<h1>Scheduled tweets</h1>";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <a href=\"#\" class=\"new_tweet button icon new\">New tweet</a>
        ";
        // line 11
        if ((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data"))) {
            // line 12
            echo "            <ul class=\"schedule\">
                ";
            // line 13
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")));
            foreach ($context['_seq'] as $context["_key"] => $context["tweet"]) {
                // line 14
                echo "                    <li>
                        <div class=\"time\">
                            <p class=\"icon date\">";
                // line 16
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["tweet"]) ? $context["tweet"] : $this->getContext($context, "tweet")), "publish"), "d/m/Y"), "html", null, true);
                echo "</p>
                            <p class=\"icon time\">";
                // line 17
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["tweet"]) ? $context["tweet"] : $this->getContext($context, "tweet")), "publish"), "H:i"), "html", null, true);
                echo "</p>
                        </div>
                        <div class=\"tweet\">
                            <p>";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tweet"]) ? $context["tweet"] : $this->getContext($context, "tweet")), "content"), "html", null, true);
                echo "</p>
                        </div>
                        <div class=\"actions\">
                            ";
                // line 23
                if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
                    // line 24
                    echo "                                <a data-id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tweet"]) ? $context["tweet"] : $this->getContext($context, "tweet")), "id"), "html", null, true);
                    echo "\" data-path=\"";
                    echo $this->env->getExtension('routing')->getPath("edit");
                    echo "\"
                                    class=\"button small icon edit\" href=\"javascript:void(0)\">Edit</a>
                                <a data-id=\"";
                    // line 26
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tweet"]) ? $context["tweet"] : $this->getContext($context, "tweet")), "id"), "html", null, true);
                    echo "\" data-path=\"";
                    echo $this->env->getExtension('routing')->getPath("delete");
                    echo "\" 
                                    class=\"button small secondary icon delete\" href=\"javascript:void(0)\">Delete</a>
                            ";
                }
                // line 29
                echo "                        </div>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tweet'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "            </ul>
        ";
        } else {
            // line 34
            echo "            <div style=\"padding: 20px 10px 20px 10px; margin-bottom: 20px; border-radius: 5px; text-align: center; clear: both; background-color: white;\">No tweets scheduled</div>
        ";
        }
    }

    // line 38
    public function block_contentTitle2($context, array $blocks = array())
    {
        echo "<h2>Download our PDF guides</h2>";
    }

    // line 39
    public function block_content2($context, array $blocks = array())
    {
        // line 40
        echo "        <a href=\"#\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner-besttools.jpg"), "html", null, true);
        echo "\" alt=\"20 Best Twitter Tools\" class=\"left\" /></a>
        <a href=\"#\"><img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner-besttools02.jpg"), "html", null, true);
        echo "\" alt=\"20 Best Twitter Tools\" class=\"left\" /></a>
        <a href=\"#\"><img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/banner-premium.jpg"), "html", null, true);
        echo "\" alt=\"Why go premium?\" /></a>
";
    }

    public function getTemplateName()
    {
        return "WebinyTweetBundle:Default:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 42,  136 => 41,  131 => 40,  128 => 39,  122 => 38,  116 => 34,  112 => 32,  104 => 29,  96 => 26,  88 => 24,  86 => 23,  80 => 20,  74 => 17,  70 => 16,  66 => 14,  62 => 13,  59 => 12,  57 => 11,  54 => 10,  51 => 9,  45 => 7,  39 => 5,  33 => 3,);
    }
}
