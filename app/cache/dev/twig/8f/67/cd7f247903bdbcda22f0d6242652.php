<?php

/* AcmeDemoBundle:Demo:contact.html.twig */
class __TwigTemplate_8f67cd7f247903bdbcda22f0d6242652 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AcmeDemoBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Symfony - Contact form";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <form action=\"";
        echo $this->env->getExtension('routing')->getPath("_demo_contact");
        echo "\" method=\"POST\" id=\"contact_form\">
        ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

        ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email"), 'row');
        echo "
        ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message"), 'row');
        echo "

        ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
        <input type=\"submit\" value=\"Send\" class=\"symfony-button-grey\" />
    </form>
";
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Demo:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 11,  77 => 22,  242 => 58,  237 => 57,  226 => 47,  216 => 43,  206 => 36,  200 => 34,  190 => 76,  170 => 60,  167 => 59,  146 => 32,  127 => 24,  124 => 23,  118 => 20,  113 => 19,  65 => 17,  104 => 29,  70 => 16,  34 => 5,  23 => 3,  100 => 27,  81 => 24,  58 => 13,  20 => 1,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 60,  247 => 59,  241 => 77,  229 => 73,  220 => 44,  214 => 69,  177 => 65,  169 => 60,  140 => 27,  132 => 25,  128 => 39,  107 => 36,  61 => 13,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  119 => 42,  102 => 32,  71 => 19,  67 => 20,  63 => 16,  59 => 12,  38 => 6,  94 => 28,  89 => 20,  85 => 25,  75 => 17,  68 => 14,  56 => 9,  87 => 9,  21 => 2,  26 => 12,  93 => 11,  88 => 24,  78 => 4,  46 => 11,  27 => 5,  44 => 8,  31 => 4,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 33,  142 => 59,  138 => 54,  136 => 26,  121 => 46,  117 => 44,  105 => 14,  91 => 27,  62 => 13,  49 => 10,  24 => 7,  25 => 3,  19 => 1,  79 => 18,  72 => 22,  69 => 19,  47 => 11,  40 => 7,  37 => 10,  22 => 3,  246 => 90,  157 => 54,  145 => 46,  139 => 45,  131 => 40,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 36,  101 => 13,  98 => 31,  96 => 12,  83 => 25,  74 => 17,  66 => 14,  55 => 14,  52 => 10,  50 => 10,  43 => 7,  41 => 9,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 35,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 61,  168 => 72,  164 => 58,  162 => 57,  154 => 34,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 44,  122 => 38,  116 => 34,  112 => 32,  109 => 34,  106 => 36,  103 => 28,  99 => 31,  95 => 28,  92 => 21,  86 => 23,  82 => 22,  80 => 20,  73 => 19,  64 => 17,  60 => 29,  57 => 12,  54 => 12,  51 => 9,  48 => 9,  45 => 8,  42 => 9,  39 => 5,  36 => 4,  33 => 3,  30 => 2,);
    }
}
