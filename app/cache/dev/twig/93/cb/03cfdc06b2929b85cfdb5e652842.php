<?php

/* WebinyTweetBundle:Default:index.html.twig */
class __TwigTemplate_93cb03cfdc06b2929b85cfdb5e652842 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'bodyClassName' => array($this, 'block_bodyClassName'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_bodyClassName($context, array $blocks = array())
    {
        echo "homepage variant1";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"wrap\">
        <div class=\"box\">
            <h1>No more complication to schedule your tweets.</h1>
            <p><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("dashboard");
        echo "\" 
                  alt=\"Sign in with Twitter\" class=\"button big orange\">
                    Sign in with Twitter
                </a>
            </p>
        </div>
    </wrap>
";
    }

    public function getTemplateName()
    {
        return "WebinyTweetBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 8,  38 => 5,  35 => 4,  29 => 3,);
    }
}
