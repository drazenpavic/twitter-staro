<?php

/* base.html.twig */
class __TwigTemplate_5fdef17cfaae2cb8f3783c546cccdfe9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'meta' => array($this, 'block_meta'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'icon' => array($this, 'block_icon'),
            'javascripts' => array($this, 'block_javascripts'),
            'bodyClassName' => array($this, 'block_bodyClassName'),
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
            'contentTitle2' => array($this, 'block_contentTitle2'),
            'content2' => array($this, 'block_content2'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    ";
        // line 4
        $this->displayBlock('meta', $context, $blocks);
        // line 7
        echo " 

    <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 18
        echo "
    ";
        // line 19
        $this->displayBlock('icon', $context, $blocks);
        // line 21
        echo "    

    ";
        // line 23
        $this->displayBlock('javascripts', $context, $blocks);
        // line 29
        echo "</head>


<body class=\"";
        // line 32
        $this->displayBlock('bodyClassName', $context, $blocks);
        echo "\"> 
";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 81
        echo "        
</body>  
</html>
";
    }

    // line 4
    public function block_meta($context, array $blocks = array())
    {
        echo " 
        <meta charset=\"UTF-8\" >
        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
    ";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/smoothness/jquery-ui-1.10.3.custom.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/smoothness/jquery-ui-1.10.3.custom.min.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        <!--[if lte IE 9]><link rel=\"Stylesheet\" type=\"text/css\" href=\"css/fix-ie9.css\" media=\"screen\" /><![endif]-->
        <!--[if lte IE 8]><link rel=\"Stylesheet\" type=\"text/css\" href=\"css/fix-ie8.css\" media=\"screen\" /><![endif]-->
    ";
    }

    // line 19
    public function block_icon($context, array $blocks = array())
    {
        echo " 
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    ";
    }

    // line 23
    public function block_javascripts($context, array $blocks = array())
    {
        // line 24
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui-1.10.3.custom.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui-1.10.3.custom.min.js"), "html", null, true);
        echo "\"></script>  
        <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/drazen.js"), "html", null, true);
        echo "\"></script>  
    ";
    }

    // line 32
    public function block_bodyClassName($context, array $blocks = array())
    {
    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        // line 34
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 54
        echo "
    <div id=\"container\" class=\"wrap\">
        <div id=\"content\" class=\"column\">
                ";
        // line 57
        $this->displayBlock('contentTitle', $context, $blocks);
        // line 58
        echo "                ";
        $this->displayBlock('content', $context, $blocks);
        // line 59
        echo "                ";
        $this->displayBlock('contentTitle2', $context, $blocks);
        // line 60
        echo "                ";
        $this->displayBlock('content2', $context, $blocks);
        // line 61
        echo "        </div>   
            
            <div id=\"column\" class=\"column\">
                <nav>   
                    <ul>
                        <li><a class=\"new_tweet icon new\" href=\"#\">New tweet</a></li>
                        <li><a class=\"icon schedule\" href=\"#\">Scheduled tweets</a></li>
                        <li><a class=\"icon feeds\" href=\"#\">My feeds</a></li>
                        <li><a class=\"icon stats\" href=\"#\">Stats</a></li>
                    </ul>
                </nav>
                <a href=\"#\" class=\"button orange\">Go Premium</a>
            </div>  
    </div>

    ";
        // line 76
        echo twig_include($this->env, $context, "WebinyTweetBundle:include:new-tweet.html.twig");
        echo "

    <div id=\"footer\">
         &copy; SKGDR 2013. <a href=\"#\">Terms of Service</a> &bull; <a href=\"#\">Contact</a>
    </div>
";
    }

    // line 34
    public function block_header($context, array $blocks = array())
    {
        // line 35
        echo "        <span class=\"loading\">
            <img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/ajax-loader.gif"), "html", null, true);
        echo "\" alt=\"ajax-loader\"/>
        </span>
        <header class=\"top\">
            <a href=\"#\" id=\"logo\">SKGDR</a>
            <div class=\"right\">
                    <a href=\"#\" class=\"icon switch\">Swith user <- Šta bi ovo trebalo bit?</a>
                    <div class=\"user\">
                            <img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/avatar.jpg"), "html", null, true);
        echo "\" alt=\"Goran Candrlic\" />
                            <span>";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "username"), "html", null, true);
        echo "</span>
                            <ul>
                                    <li><a class=\"icon settings\" href=\"#\">Account settings</a></li>
                                    <li><a class=\"icon logout\" href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\">Sign out</a></li>
                            </ul>
                            <span class=\"drop\"></span>
                    </div>
            </div>
        </header>
    ";
    }

    // line 57
    public function block_contentTitle($context, array $blocks = array())
    {
    }

    // line 58
    public function block_content($context, array $blocks = array())
    {
    }

    // line 59
    public function block_contentTitle2($context, array $blocks = array())
    {
    }

    // line 60
    public function block_content2($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  252 => 60,  247 => 59,  242 => 58,  237 => 57,  226 => 47,  220 => 44,  216 => 43,  206 => 36,  203 => 35,  200 => 34,  190 => 76,  173 => 61,  170 => 60,  167 => 59,  164 => 58,  162 => 57,  157 => 54,  154 => 34,  151 => 33,  146 => 32,  140 => 27,  136 => 26,  132 => 25,  127 => 24,  124 => 23,  118 => 20,  113 => 19,  105 => 14,  101 => 13,  96 => 12,  93 => 11,  87 => 9,  78 => 4,  71 => 81,  69 => 33,  65 => 32,  60 => 29,  58 => 23,  54 => 21,  52 => 19,  49 => 18,  47 => 11,  42 => 9,  36 => 4,  31 => 1,  43 => 8,  38 => 7,  35 => 4,  29 => 3,);
    }
}
