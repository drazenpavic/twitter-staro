<?php

/* ::base.html.twig */
class __TwigTemplate_bcfd0c1fd00fc7b7646c24a8f58607eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'meta' => array($this, 'block_meta'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'icon' => array($this, 'block_icon'),
            'javascripts' => array($this, 'block_javascripts'),
            'bodyClassName' => array($this, 'block_bodyClassName'),
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'contentTitle' => array($this, 'block_contentTitle'),
            'content' => array($this, 'block_content'),
            'contentTitle2' => array($this, 'block_contentTitle2'),
            'content2' => array($this, 'block_content2'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    ";
        // line 4
        $this->displayBlock('meta', $context, $blocks);
        // line 7
        echo " 

    <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 18
        echo "
    ";
        // line 19
        $this->displayBlock('icon', $context, $blocks);
        // line 21
        echo "    

    ";
        // line 23
        $this->displayBlock('javascripts', $context, $blocks);
        // line 29
        echo "</head>


<body class=\"";
        // line 32
        $this->displayBlock('bodyClassName', $context, $blocks);
        echo "\"> 
";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 81
        echo "        
</body>  
</html>
";
    }

    // line 4
    public function block_meta($context, array $blocks = array())
    {
        echo " 
        <meta charset=\"UTF-8\" >
        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
    ";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/smoothness/jquery-ui-1.10.3.custom.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/smoothness/jquery-ui-1.10.3.custom.min.css"), "html", null, true);
        echo "\" type=\"text/css\" />
        <!--[if lte IE 9]><link rel=\"Stylesheet\" type=\"text/css\" href=\"css/fix-ie9.css\" media=\"screen\" /><![endif]-->
        <!--[if lte IE 8]><link rel=\"Stylesheet\" type=\"text/css\" href=\"css/fix-ie8.css\" media=\"screen\" /><![endif]-->
    ";
    }

    // line 19
    public function block_icon($context, array $blocks = array())
    {
        echo " 
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    ";
    }

    // line 23
    public function block_javascripts($context, array $blocks = array())
    {
        // line 24
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui-1.10.3.custom.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui-1.10.3.custom.min.js"), "html", null, true);
        echo "\"></script>  
        <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/drazen.js"), "html", null, true);
        echo "\"></script>  
    ";
    }

    // line 32
    public function block_bodyClassName($context, array $blocks = array())
    {
    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        // line 34
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 54
        echo "
    <div id=\"container\" class=\"wrap\">
        <div id=\"content\" class=\"column\">
                ";
        // line 57
        $this->displayBlock('contentTitle', $context, $blocks);
        // line 58
        echo "                ";
        $this->displayBlock('content', $context, $blocks);
        // line 59
        echo "                ";
        $this->displayBlock('contentTitle2', $context, $blocks);
        // line 60
        echo "                ";
        $this->displayBlock('content2', $context, $blocks);
        // line 61
        echo "        </div>   
            
            <div id=\"column\" class=\"column\">
                <nav>   
                    <ul>
                        <li><a class=\"new_tweet icon new\" href=\"#\">New tweet</a></li>
                        <li><a class=\"icon schedule\" href=\"#\">Scheduled tweets</a></li>
                        <li><a class=\"icon feeds\" href=\"#\">My feeds</a></li>
                        <li><a class=\"icon stats\" href=\"#\">Stats</a></li>
                    </ul>
                </nav>
                <a href=\"#\" class=\"button orange\">Go Premium</a>
            </div>  
    </div>

    ";
        // line 76
        echo twig_include($this->env, $context, "WebinyTweetBundle:include:new-tweet.html.twig");
        echo "

    <div id=\"footer\">
         &copy; SKGDR 2013. <a href=\"#\">Terms of Service</a> &bull; <a href=\"#\">Contact</a>
    </div>
";
    }

    // line 34
    public function block_header($context, array $blocks = array())
    {
        // line 35
        echo "        <span class=\"loading\">
            <img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/ajax-loader.gif"), "html", null, true);
        echo "\" alt=\"ajax-loader\"/>
        </span>
        <header class=\"top\">
            <a href=\"#\" id=\"logo\">SKGDR</a>
            <div class=\"right\">
                    <a href=\"#\" class=\"icon switch\">Swith user <- Šta bi ovo trebalo bit?</a>
                    <div class=\"user\">
                            <img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/avatar.jpg"), "html", null, true);
        echo "\" alt=\"Goran Candrlic\" />
                            <span>";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "username"), "html", null, true);
        echo "</span>
                            <ul>
                                    <li><a class=\"icon settings\" href=\"#\">Account settings</a></li>
                                    <li><a class=\"icon logout\" href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\">Sign out</a></li>
                            </ul>
                            <span class=\"drop\"></span>
                    </div>
            </div>
        </header>
    ";
    }

    // line 57
    public function block_contentTitle($context, array $blocks = array())
    {
    }

    // line 58
    public function block_content($context, array $blocks = array())
    {
    }

    // line 59
    public function block_contentTitle2($context, array $blocks = array())
    {
    }

    // line 60
    public function block_content2($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1352 => 388,  1343 => 387,  1341 => 386,  1338 => 385,  1322 => 381,  1315 => 380,  1313 => 379,  1310 => 378,  1287 => 374,  1262 => 373,  1260 => 372,  1257 => 371,  1245 => 366,  1240 => 365,  1238 => 364,  1235 => 363,  1226 => 357,  1220 => 355,  1217 => 354,  1212 => 353,  1210 => 352,  1207 => 351,  1200 => 346,  1191 => 344,  1187 => 343,  1184 => 342,  1181 => 341,  1179 => 340,  1176 => 339,  1168 => 335,  1166 => 334,  1163 => 333,  1157 => 329,  1151 => 327,  1148 => 326,  1146 => 325,  1143 => 324,  1134 => 319,  1132 => 318,  1109 => 317,  1106 => 316,  1103 => 315,  1100 => 314,  1097 => 313,  1094 => 312,  1091 => 311,  1089 => 310,  1086 => 309,  1079 => 305,  1075 => 304,  1070 => 303,  1068 => 302,  1065 => 301,  1058 => 296,  1055 => 295,  1047 => 290,  1044 => 289,  1042 => 288,  1039 => 287,  1031 => 282,  1027 => 281,  1023 => 280,  1020 => 279,  1018 => 278,  1015 => 277,  1007 => 273,  1005 => 269,  1003 => 268,  1000 => 267,  995 => 263,  973 => 258,  970 => 257,  967 => 256,  964 => 255,  961 => 254,  958 => 253,  955 => 252,  952 => 251,  949 => 250,  946 => 249,  943 => 248,  941 => 247,  938 => 246,  930 => 240,  927 => 239,  925 => 238,  922 => 237,  914 => 233,  911 => 232,  909 => 231,  906 => 230,  894 => 226,  891 => 225,  888 => 224,  885 => 223,  883 => 222,  880 => 221,  872 => 217,  869 => 216,  867 => 215,  864 => 214,  856 => 210,  853 => 209,  851 => 208,  848 => 207,  840 => 203,  837 => 202,  835 => 201,  832 => 200,  824 => 196,  821 => 195,  819 => 194,  816 => 193,  808 => 189,  805 => 188,  800 => 186,  789 => 181,  787 => 180,  776 => 175,  774 => 174,  763 => 169,  760 => 168,  758 => 167,  755 => 166,  747 => 162,  744 => 161,  740 => 159,  737 => 158,  730 => 153,  720 => 152,  715 => 151,  712 => 150,  703 => 147,  701 => 146,  688 => 138,  687 => 137,  685 => 135,  680 => 134,  674 => 132,  671 => 131,  669 => 130,  666 => 129,  657 => 123,  653 => 122,  649 => 121,  640 => 119,  634 => 117,  631 => 116,  629 => 115,  626 => 114,  610 => 110,  608 => 109,  605 => 108,  589 => 104,  587 => 103,  584 => 102,  567 => 98,  555 => 96,  548 => 93,  546 => 92,  541 => 91,  538 => 90,  518 => 88,  506 => 82,  503 => 81,  500 => 80,  494 => 78,  492 => 77,  487 => 76,  484 => 75,  481 => 74,  471 => 72,  459 => 69,  456 => 68,  450 => 64,  442 => 62,  433 => 60,  428 => 59,  426 => 58,  414 => 52,  405 => 49,  403 => 48,  400 => 47,  390 => 43,  388 => 42,  385 => 41,  377 => 37,  366 => 33,  350 => 26,  316 => 16,  313 => 15,  311 => 14,  308 => 13,  299 => 8,  271 => 371,  266 => 363,  260 => 360,  250 => 338,  245 => 332,  225 => 295,  215 => 277,  186 => 236,  129 => 145,  126 => 144,  114 => 108,  356 => 328,  339 => 316,  295 => 275,  806 => 488,  803 => 187,  792 => 182,  788 => 484,  784 => 179,  771 => 173,  745 => 476,  742 => 160,  723 => 473,  706 => 148,  702 => 470,  698 => 145,  694 => 468,  690 => 139,  686 => 136,  682 => 465,  678 => 464,  675 => 463,  673 => 462,  656 => 461,  645 => 120,  630 => 455,  625 => 453,  621 => 452,  618 => 451,  616 => 450,  602 => 449,  565 => 414,  547 => 411,  530 => 410,  527 => 409,  525 => 408,  520 => 89,  515 => 87,  244 => 136,  188 => 90,  389 => 160,  386 => 159,  378 => 157,  371 => 35,  367 => 155,  358 => 151,  345 => 147,  343 => 146,  340 => 145,  334 => 141,  331 => 140,  328 => 139,  326 => 138,  307 => 128,  302 => 125,  296 => 121,  293 => 6,  290 => 5,  281 => 385,  276 => 378,  259 => 103,  253 => 339,  232 => 88,  222 => 294,  210 => 267,  184 => 230,  155 => 47,  152 => 46,  363 => 32,  357 => 123,  353 => 149,  344 => 24,  332 => 20,  327 => 114,  324 => 113,  321 => 135,  318 => 111,  306 => 107,  297 => 276,  291 => 102,  288 => 4,  274 => 110,  265 => 105,  263 => 362,  255 => 350,  231 => 83,  212 => 276,  202 => 263,  174 => 214,  175 => 58,  161 => 199,  462 => 202,  449 => 198,  446 => 197,  441 => 196,  439 => 195,  431 => 189,  429 => 188,  422 => 184,  415 => 180,  408 => 50,  401 => 172,  394 => 168,  380 => 158,  373 => 156,  361 => 152,  351 => 120,  348 => 140,  342 => 23,  338 => 135,  335 => 21,  329 => 131,  325 => 129,  323 => 128,  320 => 127,  315 => 131,  303 => 106,  300 => 105,  289 => 113,  286 => 112,  275 => 105,  270 => 102,  267 => 101,  262 => 98,  256 => 96,  248 => 333,  233 => 301,  213 => 78,  207 => 266,  197 => 246,  194 => 245,  191 => 243,  185 => 74,  181 => 229,  178 => 59,  172 => 57,  165 => 83,  153 => 77,  150 => 55,  134 => 158,  97 => 41,  110 => 38,  90 => 27,  84 => 41,  76 => 31,  53 => 11,  77 => 25,  242 => 58,  237 => 57,  226 => 47,  216 => 43,  206 => 36,  200 => 34,  190 => 76,  170 => 60,  167 => 59,  146 => 32,  127 => 24,  124 => 23,  118 => 20,  113 => 19,  65 => 32,  104 => 87,  70 => 19,  34 => 4,  23 => 3,  100 => 36,  81 => 30,  58 => 23,  20 => 1,  480 => 162,  474 => 161,  469 => 71,  461 => 70,  457 => 153,  453 => 199,  444 => 149,  440 => 148,  437 => 61,  435 => 146,  430 => 144,  427 => 143,  423 => 57,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 164,  384 => 121,  381 => 120,  379 => 119,  374 => 36,  368 => 34,  365 => 111,  362 => 110,  360 => 109,  355 => 27,  341 => 118,  337 => 22,  322 => 101,  314 => 99,  312 => 130,  309 => 129,  305 => 95,  298 => 120,  294 => 90,  285 => 3,  283 => 115,  278 => 384,  268 => 370,  264 => 84,  258 => 351,  252 => 60,  247 => 59,  241 => 93,  229 => 87,  220 => 44,  214 => 69,  177 => 65,  169 => 207,  140 => 27,  132 => 25,  128 => 39,  107 => 37,  61 => 2,  273 => 377,  269 => 107,  254 => 92,  243 => 324,  240 => 323,  238 => 309,  235 => 308,  230 => 300,  227 => 298,  224 => 81,  221 => 77,  219 => 76,  217 => 286,  208 => 76,  204 => 264,  179 => 221,  159 => 193,  143 => 51,  135 => 62,  119 => 114,  102 => 30,  71 => 81,  67 => 16,  63 => 21,  59 => 17,  38 => 7,  94 => 57,  89 => 35,  85 => 26,  75 => 22,  68 => 20,  56 => 12,  87 => 9,  21 => 2,  26 => 3,  93 => 11,  88 => 28,  78 => 4,  46 => 10,  27 => 4,  44 => 8,  31 => 1,  28 => 3,  201 => 92,  196 => 92,  183 => 82,  171 => 213,  166 => 206,  163 => 82,  158 => 80,  156 => 192,  151 => 33,  142 => 59,  138 => 54,  136 => 26,  121 => 128,  117 => 39,  105 => 14,  91 => 56,  62 => 14,  49 => 18,  24 => 2,  25 => 35,  19 => 1,  79 => 32,  72 => 21,  69 => 33,  47 => 11,  40 => 11,  37 => 7,  22 => 2,  246 => 96,  157 => 54,  145 => 74,  139 => 166,  131 => 157,  123 => 61,  120 => 31,  115 => 40,  111 => 107,  108 => 33,  101 => 13,  98 => 29,  96 => 12,  83 => 30,  74 => 20,  66 => 12,  55 => 12,  52 => 19,  50 => 10,  43 => 9,  41 => 7,  35 => 5,  32 => 6,  29 => 4,  209 => 82,  203 => 35,  199 => 262,  193 => 73,  189 => 237,  187 => 75,  182 => 87,  176 => 220,  173 => 61,  168 => 61,  164 => 58,  162 => 57,  154 => 34,  149 => 179,  147 => 75,  144 => 173,  141 => 172,  133 => 55,  130 => 46,  125 => 42,  122 => 41,  116 => 113,  112 => 39,  109 => 102,  106 => 101,  103 => 28,  99 => 68,  95 => 34,  92 => 31,  86 => 46,  82 => 25,  80 => 24,  73 => 23,  64 => 19,  60 => 29,  57 => 19,  54 => 21,  51 => 37,  48 => 11,  45 => 9,  42 => 9,  39 => 10,  36 => 4,  33 => 4,  30 => 3,);
    }
}
