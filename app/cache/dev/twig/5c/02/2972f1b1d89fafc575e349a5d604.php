<?php

/* WebinyTweetBundle:include:new-tweet.html.twig */
class __TwigTemplate_5c022972f1b1d89fafc575e349a5d604 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"shade\">
    <div class=\"popup box\" style=\"display: block\">
         <form id=\"newtweet\" action=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("publish");
        echo "\"> 
            <h2><div id=\"NewOrEdit\">New Tweet</div></h2>  
            <fieldset>
                <input type=\"hidden\" name=\"tweetID\" id=\"tweetID\" >
                <textarea name=\"content\" id=\"scheduleTweet\" cols=\"30\" rows=\"3\"></textarea>
                <label for=\"scheduleDate\" class=\"icon date\">Date</label>
                <input type=\"text\" name=\"publish\" id=\"scheduleDate\" >
                <label for=\"scheduleHour\" class=\"icon time\">Time</label>
                <select id=\"scheduleHour\" name=\"hour\"><option>00</option><option>01</option><option>02</option><option>03</option><option>04</option><option>05</option><option>06</option><option>07</option><option>08</option><option>09</option><option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option><option>16</option><option>17</option><option>18</option><option>19</option><option>20</option><option>21</option><option>22</option><option>23</option></select>
                <select id=\"scheduleMinute\" name=\"minute\"><option>00</option><option>05</option><option>10</option><option>15</option><option>20</option><option>25</option><option>30</option><option>35</option><option>40</option><option>45</option><option>50</option><option>55</option></select>
                <button type=\"submit\">Schedule tweet</button>
            </fieldset>
        </form>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "WebinyTweetBundle:include:new-tweet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,  140 => 42,  136 => 41,  131 => 40,  128 => 39,  122 => 38,  116 => 34,  112 => 32,  104 => 29,  96 => 26,  88 => 24,  86 => 23,  80 => 20,  74 => 17,  70 => 16,  66 => 14,  62 => 13,  59 => 12,  57 => 11,  54 => 10,  51 => 9,  45 => 7,  39 => 5,  33 => 3,);
    }
}
